import React, { Component } from "react";

export default class ItemShoe extends Component {
  render() {
    let { image, name, price } = this.props.data;

    // Truncate the title if it's more than 2 lines
    const truncatedName = name.length > 25 ? name.substring(0, 25) + "..." : name;

    return (
      <div className="col-3 m-5 ">
        <div
          className="card d-flex flex-column justify-content-between shadow-lg p-3 mb-5 bg-white rounded"
          style={{ height: "100%", width: "15rem" }}
        >
          <img
            className="card-img-top w-100 h-50 shadow-lg p-3 mb-5 bg-white rounded"
            src={image}
            alt={`${name} shoe`}
          />
          <div className="card-body d-flex flex-column">
            <h5 className="card-title mb-2" style={{ minHeight: "3rem", maxHeight: "6rem", overflow: "hidden" }}>
              {truncatedName}
            </h5>
            <p className="card-text">{price}$</p>
            <button
              onClick={() => {
                this.props.handleAddToCart(this.props.data);
              }}
              className="btn btn-outline-dark shadow mt-auto"
            >
              Add to cart <i className="fa fa-cart-plus"></i>
            </button>
          </div>
        </div>
      </div>
    );
  }
}