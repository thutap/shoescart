import React, { Component } from "react";

export default class GioHang extends Component {
  rendertbody = () => {
    return this.props.gioHang.map((item, index) => {
      return (
        <tr key={index}>
          
          <td style={{ textAlign: "center", verticalAlign: "middle" }}>{item.name}</td>
          <td style={{ textAlign: "center", verticalAlign: "middle" }}>{item.price}$</td>

          <td style={{ textAlign: "center", verticalAlign: "middle" }}>
            <img style={{ width: 100 }} src={item.image} alt="" />
          </td>
          <td style={{ textAlign: "center", verticalAlign: "middle" }}>
            <button
              onClick={() => {
                this.props.handleChangeQuantity(item.id, -1);
              }}
              className="btn btn-outline-warning"
            >
              -
            </button>
            <span className="px-5">{item.soLuong}</span>
            <button
              onClick={() => {
                this.props.handleChangeQuantity(item.id, 1);
              }}
              className="btn btn-outline-warning"
            >
              +
            </button>
          </td>
        </tr>
      );
    });
  };

  render() {
    return (
      <div>
        <table className="table">
          <thead>
            <tr>
             
              <th style={{ textAlign: "center" }}>Tên</th>
              <th style={{ textAlign: "center" }}>Giá</th>
              <th style={{ textAlign: "center" }}>Hình Ảnh</th>
              <th style={{ textAlign: "center" }}>Số Lượng</th>
            </tr>
          </thead>
          <tbody>{this.rendertbody()}</tbody>
        </table>
        <p style={{ textAlign: "center" }}>Total Price: {this.props.totalPrice}$</p>
      </div>
    );
  }
}