import React, { useState, useEffect } from 'react';
import { NavLink } from 'react-router-dom';
import { Button, notification, Modal } from 'antd';
import GioHang from '../components/GioHang';

export default function GiohangPage() {
  const [gioHang, setGioHang] = useState([]);
  const [open, setOpen] = useState(false);
  const [confirmLoading, setConfirmLoading] = useState(false);
  const [modalText, setModalText] = useState('Bạn Muốn Thanh toán !!!');

  useEffect(() => {
    // Fetch shopping cart data from local storage on component mount
    const cartData = JSON.parse(localStorage.getItem('gioHang')) || [];
    setGioHang(cartData);

    // Set up event listener for changes in local storage
    const handleStorageChange = (e) => {
      if (e.key === 'gioHang') {
        const updatedCart = JSON.parse(e.newValue) || [];
        setGioHang(updatedCart);
      }
    };

    // Add event listener
    window.addEventListener('storage', handleStorageChange);

    // Clean up the event listener when the component unmounts
    return () => {
      window.removeEventListener('storage', handleStorageChange);
    };
  }, []); 

  const saveCartToLocalStorage = (cartData) => {
    localStorage.setItem('gioHang', JSON.stringify(cartData || []));
  };

  const calculateTotalPrice = () => {
    return gioHang.reduce((total, item) => total + item.soLuong * item.price, 0);
  };

  const handleClearCart = () => {
    setGioHang([]);
    saveCartToLocalStorage([]); // Clear and save the shopping cart in state

    // Additionally, clear the cart in local storage
    localStorage.removeItem('gioHang');
  };

  const handlePayment = () => {
    setOpen(true);
 
  };
  const handleOk = () => {
    setModalText('Processing payment...');
    setConfirmLoading(true);
  
    setTimeout(() => {
      setOpen(false);
      setConfirmLoading(false);
  
      handleClearCart(); // Gọi handleClearCart trước khi hiển thị notification
  
      setTimeout(() => {
        notification.success({
          message: "Thanh toán thành công.",
          description: "Cảm ơn bạn đã mua hàng!",
          style: { top: 24, right: 24 },
        });
      }, 500); // Thời gian trễ để đảm bảo handleClearCart đã được gọi xong
    }, 2000);
  };
  const handleCancel = () => {
    notification.info({
      message: "Thanh toán bị hủy.",
      style: { top: 24, right: 24 },
    });
    setOpen(false);
  };

  const handleChangeQuantity = (idShoe, quantityChange) => {
    setGioHang((prevGioHang) => {
      const updatedGioHang = prevGioHang
        .map((shoe) => {
          if (shoe.id === idShoe) {
            const newQuantity = shoe.soLuong + quantityChange;

            // Loại bỏ sản phẩm nếu số lượng trở thành 0 hoặc âm
            if (newQuantity <= 0) {
              return null; // Signal to filter out this item
            }

            return { ...shoe, soLuong: newQuantity };
          }
          return shoe;
        })
        .filter(Boolean); // Filter out null entries

      // Lưu vào local storage
      saveCartToLocalStorage(updatedGioHang);

      // Trả về state mới
      return updatedGioHang;
    });
  };
  

  return (
    <div className="container y-5">
      <h1 className="text-danger">Shoes Shop</h1>
      <h3>Giỏ Hàng Page</h3>
      <h5>
        <NavLink className="text" to="/home">
          Home Page
        </NavLink>{' '}
        Here!
      </h5>
      <div className="container">
        <div className="Cart">
          {gioHang.length > 0 ? (
            <div>
              <GioHang gioHang={gioHang} handleChangeQuantity={handleChangeQuantity} totalPrice={calculateTotalPrice()} />
              <Button onClick={handlePayment}>Thanh Toán</Button>
              <Modal
             title="Thanh Toán"
             open={open}
             onOk={handleOk}
             confirmLoading={confirmLoading}
             onCancel={handleCancel}
           >
             <p>{modalText}</p>
           </Modal>
            </div>
           
          ) : (
            <p>
              Giỏ hàng không có sản phẩm.{' '}
              <NavLink className="text" to="/home">
                Quay Lại Mua Hàng
              </NavLink>
            </p>
          )}
        </div>
      </div>
    </div>
  );
}